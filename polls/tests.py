import datetime
from django.utils import timezone
from django.test import TestCase
from .models import Question
from django.core.urlresolvers import reverse

class QuestionMethodsTest(TestCase):
    def test_future_question(self):
        time = timezone.now() + datetime.timedelta(days=30)
        fq = Question(pub_date=time)
        self.assertEqual(fq.recent(), False)
    def test_old_question(self):
        time = timezone.now() - datetime.timedelta(days=30)
        oq = Question(pub_date=time)
        self.assertEqual(oq.recent(), False)
    def test_recent_question(self):
        time = timezone.now() - datetime.timedelta(hours=1)
        rq = Question(pub_date=time)
        self.assertEqual(rq.recent(), True)

def createQuestion(text, days):
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=text, pub_date=time)

class QuestionViewTests(TestCase):
    def test_no_questions(self):
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context['recent_list'], [])

    def test_old_question(self):
        createQuestion('old', -30)
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'old')

    def test_future_question(self):
        createQuestion('future', 30)
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'future')

    def test_old_future_questions(self):
        createQuestion('old', -30)
        createQuestion('future', 30)
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'old')
        self.assertNotContains(response, 'future')

    def test_mul_old_questions(self):
        createQuestion('old1', -30)
        createQuestion('old2', -15)
        response = self.client.get(reverse('polls:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'old1')
        self.assertContains(response, 'old2')

class QuestionIndexDetailTests(TestCase):
    def test_detail_view_with_a_future_question(self):
        future_question = createQuestion('Future question.', 5)
        response = self.client.get(
            reverse('polls:detail', args=(future_question.id,))
        )
        self.assertEqual(response.status_code, 404)

    def test_detail_view_with_a_past_question(self):
        past_question = createQuestion('Past Question.', -5)
        response = self.client.get(
            reverse('polls:detail', args=(past_question.id,))
        )
        self.assertContains(
            response,
            past_question.question_text,
            status_code=200
        )
